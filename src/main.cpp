#include <iostream>
#include <JetsonGPIO.h>
#include <chrono>
#include <thread>
#include <bitset>

#define CS_PIN 35
#define MOSI_PIN 37
#define SCK_PIN 38

void init() {
    GPIO::setmode(GPIO::BOARD);
    GPIO::setup(40, GPIO::OUT, 0);
    GPIO::setup(SCK_PIN, GPIO::OUT, 0);
    GPIO::setup(MOSI_PIN, GPIO::OUT, 0);
    GPIO::setup(CS_PIN, GPIO::OUT, 0);
}

void wait_us(int u_seconds) {
    std::this_thread::sleep_for(std::chrono::microseconds(u_seconds));
}

void write_to_chip(std::bitset<8> &bits) {
    GPIO::output(CS_PIN, 1);
    wait_us(1);
    // uint8_t write_val;
    for (int i=0; i<8; i++) {
        // bits &= 0x01;
        // write_val = static_cast<uint8_t>(bits[i]);
        GPIO::output(MOSI_PIN, bits[i]);
        wait_us(1);
        GPIO::output(SCK_PIN, 1);
        wait_us(1);
        GPIO::output(SCK_PIN, 0);
        wait_us(1);
    }
    GPIO::output(CS_PIN, 0);
    wait_us(1);
}

void clear_595() {
    std::bitset<8> null_bits {0b00000000};
    write_to_chip(null_bits);
}

int main() {
    int wait_ms = 500;
    bool flicker = false;
    int counter = 0;
    std::string print_bits;    
    
    std::bitset<8> write_bits {0b00000001};

    init();
    clear_595();
    while(true) {
        // std::this_thread::sleep_for(std::chrono::milliseconds(wait_ms));
        GPIO::output(40, !flicker);
        flicker = !flicker;
        
        //clear_595();

        for (int i=0; i<14; i++) {
            print_bits = write_bits.to_string();

            write_to_chip(write_bits);

            std::cout << print_bits << std::endl;

            wait_us(100000);
            if (i<7) {
                write_bits <<= 1;
            } else {
                write_bits >>= 1;
            }
        }
        
        //clear_595();

        if (++counter>5) {
            clear_595();
            GPIO::cleanup();
            std::cout << "goodbye" << std::endl;
            return 0;
        }
    }
    
    GPIO::cleanup();
    return 1;

}
