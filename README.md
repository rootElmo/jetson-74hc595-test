# Jetson 74HC595 test

A quick test for serial communication with the Jetson Nano using the GPIO pins. Writes a value to the 74HC595 shift register which in turn displays it with the connected LEDs

### Schematic

![schem001](./imgs/595test001.png)
